<?php $this->load->view('templates/primary_header'); ?>
<div class="container">
    <div class="row">

        <div class="col-sm-12 col-md-12">

        <?php if(!empty($breadcrumbs) && count($breadcrumbs)>0) {?>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <?php 
            foreach($breadcrumbs as $key=>$element) {    
                ?>
                <li class="breadcrumb-item"><a href="<?php print $element;?>"><?php print $key;?></a></li>
            <?php } ?>
          </ol>
        </nav>
        <?php } ?>


            <form method="post" name="checkout" id="checkout-frm">
                
                <div class="row">    
                    
                        <div class="col-sm-4">
                            <div class="form-group">               
                                <input class="form-control input-ordernow-firstname" type="text" name="firstname" id="firstname" placeholder="First Name *">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">                
                                <input class="form-control input-ordernow-lastname" type="text" name="lastname" id="lastname" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">               
                                <input class="form-control input-ordernow-email" type="email" name="email" id="email" placeholder="Email *">
                            </div>
                        </div>
                        </div>
                        <div class="row">    
                        <div class="col-sm-4">
                            <div class="form-group">                
                                <input class="form-control input-ordernow-phone" maxlength="15" pattern="([0-9]|[0-9]|[0-9])" type="text" name="phone" id="phone" placeholder="Phone *">
                            </div>
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="form-group">                
                                <input class="form-control input-ordernow-company" type="text" name="company" id="company" placeholder="Company">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">                
                                <input class="form-control input-ordernow-address" type="text" name="address" id="address" placeholder="Address">
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                                        
                        <div class="col-sm-3">
                            <div class="form-group">
                                <select title="Select Country" name="country" class="form-control input-ordernow-country">
                                    <option value="">Select Country</option>
                                    <option value="India">India</option>
                                    <option value="USA">USA</option>
                                </select>
                                
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">                
                                <input class="form-control input-ordernow-state" type="text" name="state" id="state" placeholder="State">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">                
                                <input class="form-control input-ordernow-city" type="text" name="city" id="city" placeholder="City">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">                
                                <input class="form-control input-ordernow-zipcode" type="text" name="zipcode" id="zipcode" placeholder="ZipCode">
                            </div>
                        </div>            
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    

                            <table class="table table-bordered table-hover table-striped print-table order-table">
                                <?php if(!empty($productInfo)) { ?>
                    <thead>
                        <tr class="bg-primary">
                            <th class="text-left" style="width:45%">Product</th>
                            <th class="text-right" style="width:15%">Price</th>
                            <th class="text-right add-tax-th" style="width:15%">QTY</th>
                            <th class="text-right" style="width:25%">Sub Total</th>                          
                        </tr>
                    </thead>
                    <tbody id="render-more-item-tr">
                        <?php 
                        $grandTotal =0;   
                        foreach($productInfo as $key=>$element) { 
                        $grandTotal  += $element['subtotal'];
                            ?>
                            <tr class="count-inv-tr" id="render-more-item<?php print $element['id'];?>">
                                <td class="border-bottom" style="word-break: keep-all;">
                                    <div class="media">
                                        <a class="thumbnail pull-left" href="#"> <img class="media-object" src="<?php print HTTP_CATALOG_IMAGE_PATH.$element['image'];?>" style="width: 50px; height: 50px;"></a>
                                        <div class="media-body">
                                            <h6 class="media-heading"><a href="<?php print site_url().'product/'.$element['slug'];?>">&nbsp;<?php print $element['name']; ?></a></h6>
                                            <span style="font-size: 11px;">&nbsp;Status: </span><span class="text-success" style="font-size: 11px;"><strong>In Stock</strong></span>
                                        </div>
                                    </div>
                                    
                                </td>
                                <td class="text-right border-bottom">
                                    <span class="currency-symbol">₹</span>
                                    <span id="price0" class="dynamic-price"><?php print $element['price']; ?></span>
                                </td>
                                <td class="text-right" style="text-align: right; padding: 1px 5px;font-size: 12px; line-height: 1.5; 
                                    border-radius: 3px;">
                                        <?php print $element['qty']; ?>
                                </td>
                                <td class="text-right border-bottom">
                                    <span class="currency-symbol">₹</span>
                                    <span><?php print $element['subtotal']; ?></span>
                                </td>            
                            </tr>
                             <?php } ?>
                         </tbody>

                                                 
                        <tfoot id="render-calculation">  
                            <tr>            
                                <td class="add-tax-colspan" style="text-align: right; border:0px;"></td>
                                <td class="text-left" style="width:40%; border-right: 0;" colspan="2">
                                    <strong>Subtotal</strong>
                                </td>
                                <td class="text-right" style="border-left: 0px;">
                                    <strong>
                                        <span class="currency-symbol">₹ </span>
                                        <span id="sub-total"><?php print $grandTotal;?></span>
                                    </strong>
                                </td>           
                            </tr>
                               
                                                        <tr id="render-tax">
                                <td style="text-align:left; border:0px;"></td>
                                <td style="text-align:left; border-right: 0px;" colspan="2"><strong id="change-tax-label">GST @0%</strong></td>
                                <td style="text-align:right; border-left: 0px;">
                                    <span id="tax-total" style="visibility: hidden;">0.00</span>
                                </td>
                            </tr>

                            <tr id="render-tax-cgst">
                                <td style="text-align:left; border:0px;"></td>
                                <td style="text-indent: 30px; border-right: 0px;" colspan="2"><strong>- CGST @0%</strong></td>
                                <td style="text-align:right; border-left: 0px;"><strong><span>₹ </span><span>0.00</span></strong></td>
                            </tr>
                            <tr id="render-tax-sgst">
                                <td style="text-align:left; border:0px;"></td>
                                <td style="text-indent: 30px; border-right: 0px;" colspan="2"><strong>- SGST @0%</strong></td>
                                <td style="text-align:right; border-left: 0px;"><strong><span>₹ </span><span>0.00</span></strong></td>
                            </tr>
                            <tr>            
                                <td class="add-tax-colspan" style="text-align: right; border:0px;"></td>            
                                <td class="text-left" colspan="2" style="border-right: 0px;">
                                    <strong>Total</strong>
                                </td>
                                <td class="border-bottom text-right" style="border-left: 0px;">
                                    <strong>
                                        <span class="currency-symbol">₹ </span>
                                        <span id="final-total"><?php print $grandTotal;?></span>
                                    </strong>
                                </td>
                            </tr>

                        </tfoot>
                    <?php } else { ?>
                        <tr><td colspan="4">Cart is empty.</td></tr>
                    <?php }?>
                    
                </table>

                        


                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                        <?php if(!empty($productInfo)) { ?>
                            <a href="<?php print site_url();?>cart" class="btn btn-success">Back</a>
                            <button type="button" class="btn btn-warning" id="order-pay-now">Pay Now</button>
                        <?php } else { ?>
                            <a href="<?php print site_url();?>" class="btn btn-success">Continue Shopping</a>
                        <?php }?>
                    </div>  
                </div>
            </form>




                    </div>
    </div>
</div>

<?php $this->load->view('templates/primary_footer'); ?>