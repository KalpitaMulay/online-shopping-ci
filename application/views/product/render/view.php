<div class="modal-header">
                <h3 class="modal-title"><?php print $productInfo['name'];?></h3>
                <a href="#" data-dismiss="modal" class="class pull-right"><i class="fa fa-times"></i></a>
                
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 product_img">
                        <img src="<?php print HTTP_CATALOG_IMAGE_PATH.$productInfo['image'];?>" class="img-responsive">
                    </div>
                    <div class="col-md-6 product_content">
                        <h4>SKU: <span><?php print $productInfo['sku'];?></span></h4>
                        <p><?php print $productInfo['description'];?></p>
                        <h3 class="cost">₹<?php print $productInfo['price'];?></h3>

                        <div class="space-ten"></div>
                        <div class="btn-ground">
                            <button type="button" class="btn btn-primary item-add-to-cart item-quick-add-on-cart" id="AddToCartText" data-productid="<?php print $productInfo['product_id'];?>"><i class="fa fa-shopping-cart"></i> Add To Cart</button>
                        </div>
                    </div>
                </div>
            </div>