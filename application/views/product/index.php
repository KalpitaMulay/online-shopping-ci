<?php $this->load->view('templates/primary_header'); ?>
<?php
$cart_check = $this->cart->contents();
?>

<?php if(!empty($breadcrumbs) && count($breadcrumbs)>0) {?>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <?php foreach($breadcrumbs as $key=>$element) {?>
                <li class="breadcrumb-item"><a href="<?php print $element;?>"><?php print $key;?></a></li>
            <?php } ?>
          </ol>
        </nav>
        <?php } ?>

<div class="container">
    <div class="row">
<?php if(!empty($products)) { ?>
    <?php foreach($products as $key=>$element) {?>

        <div class="col-md-3 col-sm-6">
            <div class="product-grid">
                <div class="product-image">
                    <a href="<?php print site_url().'product/'.$element['slug'];?>">
                        <img class="pic-1" src="<?php print HTTP_IMAGES_PATH;?>product/<?php print $element['image'];?>">
                        <img class="pic-2" src="<?php print HTTP_IMAGES_PATH;?>product/<?php print $element['image'];?>">
                    </a>
                    <ul class="social">
                        <li><a href="javascript:void(0);" class="item-quick-view" data-tip="Quick View" data-toggle="modal" data-target="#quick-view-product" data-productid="<?php print $element['product_id'];?>"><i class="fa fa-search"></i></a></li>
                        <li><a href="javascript:void(0);" class="item-add-to-cart" data-productid="<?php print $element['product_id'];?>" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                    </ul>
                    <!--span class="product-new-label">Sale</span-->
                    <!--span class="product-discount-label">20%</span-->
                </div>
                
                <div class="product-content">
                    <h3 class="title"><a href="<?php print site_url().'product/'.$element['slug'];?>"><?php print $element['name'];?></a></h3>
                    <div class="price">₹<?php print $element['price'];?>
                        <span>₹<?php print $element['price'];?></span>
                    </div>
                    <a style="cursor: pointer;" class="add-to-cart-list item-add-to-cart" data-productid="<?php print $element['product_id'];?>">+ Add To Cart</a>
                </div>
            </div>
             </div>

<?php } ?>
<?php } ?>
       </div>
    </div>
<?php $this->load->view('product/popup/view'); ?>
<?php $this->load->view('templates/primary_footer'); ?>
